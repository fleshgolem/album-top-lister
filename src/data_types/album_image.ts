import { autoserialize, autoserializeAs } from 'cerialize';

export enum SizeClass {
    small = 'small',
    medium = 'medium',
    large = 'large',
    extraLarge = 'extraLarge',
}

export class AlbumImage {
    @autoserializeAs('#text') public text: string;
    @autoserializeAs(SizeClass) public size: string;

    constructor(size: SizeClass, text: string) {
        this.size = size;
        this.text = text;
    }
}
