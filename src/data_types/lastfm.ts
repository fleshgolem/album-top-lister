import { autoserialize, autoserializeAs } from 'cerialize';
import {Album} from '@/data_types/album';

class LFMAlbumMatches {
    @autoserializeAs(Album, 'album') public albums: Album[];

    constructor(albums: Album[]) {
        this.albums = albums;
    }
}

class LFMResults {
    @autoserializeAs(LFMAlbumMatches) public albummatches: LFMAlbumMatches;

    constructor(albummatches: LFMAlbumMatches) {
        this.albummatches = albummatches;
    }
}

export class LFMResponse {
    @autoserializeAs(LFMResults) public results: LFMResults;

    constructor(results: LFMResults) {
        this.results = results;
    }
}
