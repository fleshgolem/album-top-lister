import { autoserialize, autoserializeAs } from 'cerialize';
import {AlbumImage, SizeClass} from '@/data_types/album_image';

export class Album {
    @autoserialize public name: string;
    @autoserialize public artist: string;
    @autoserialize public url: string;
    @autoserialize public streamable: string;
    @autoserialize public mbid: string;
    @autoserializeAs(AlbumImage, 'image') public images: AlbumImage[];

    constructor(name: string, artist: string, url: string, streamable: string, mbid: string) {
        this.name = name;
        this.artist = artist;
        this.url = url;
        this.streamable = streamable;
        this.mbid = mbid;
        this.images = [];
    }

    public getImage(size: SizeClass): (AlbumImage | undefined) {
        return this.images.filter((i: AlbumImage) => {
            return i.size === size;
        })[0];
    }
}
