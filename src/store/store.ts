import * as Vuex from 'vuex';
import { albums } from './albums';
import { layout } from './layout';
import { State } from './state';

export const createStore = () => {
    return new Vuex.Store<State>({
        modules: {
            albums,
            layout,
        },
    });
};
