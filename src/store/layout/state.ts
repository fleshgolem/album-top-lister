export interface LayoutState {
    rows: number;
    columns: number;
}
