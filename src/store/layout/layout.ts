import { ActionContext, Store } from 'vuex';
import { getStoreAccessors } from 'vuex-typescript';
import { State as RootState } from '../state';
import {LayoutState} from './state';

type LayoutContext = ActionContext<LayoutState, RootState>;

export const layout = {
    namespaced: true,
    state: {
        rows: 5,
        columns: 5,
    },
    getters: {
        getRows(state: LayoutState): number {
            return state.rows;
        },
        getColumns(state: LayoutState): number {
            return state.columns;
        },
    },
    mutations: {
        setRows(state: LayoutState, rows: number) {
            state.rows = rows;
        },
        setColumns(state: LayoutState, columns: number) {
            state.columns = columns;
        },
    },

    actions: {
    },
};

const { commit, read, dispatch } =
    getStoreAccessors<LayoutState, RootState>('layout'); // We pass namespace here, if we make the module namespaced: true.

export const getRows = read(layout.getters.getRows);
export const getColumns = read(layout.getters.getColumns);
export const commitSetRows = commit(layout.mutations.setRows);
export const commitSetColumns = commit(layout.mutations.setColumns);
