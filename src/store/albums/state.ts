import {Album} from '@/data_types/album';
import Vue from 'vue';

export class AlbumsContainer {
    public albums: { [index: number]: { [index: number]: Album } } = {};

    public setAlbum(row: number, column: number, album: Album) {
        let albumRow = this.albums[row];
        if (!albumRow) {
            albumRow = {};
        }
        Vue.set(albumRow, column, album);
        Vue.set(this.albums, row, albumRow);
    }

    public getAlbum(row: number, column: number): Album | undefined {

        const albumRow = this.albums[row];
        if (!albumRow) {
            return;
        }
        return albumRow[column];
    }

}

export interface AlbumsState {
    container: AlbumsContainer;
}
