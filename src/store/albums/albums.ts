import { ActionContext, Store } from 'vuex';
import { getStoreAccessors } from 'vuex-typescript';
import { State as RootState } from '../state';
import {AlbumsContainer, AlbumsState} from './state';
import {Album} from '@/data_types/album';

type AlbumsContext = ActionContext<AlbumsState, RootState>;

export const albums = {
    namespaced: true,
    state: {
        container: new AlbumsContainer(),
    },
    getters: {
        getAlbums(state: AlbumsState): AlbumsContainer {
            return state.container;
        },
    },
    mutations: {
        setAlbum(state: AlbumsState, payload: {row: number, column: number, album: Album}) {
            state.container.setAlbum(payload.row, payload.column, payload.album);
        },
    },

    actions: {
    },
};

const { commit, read, dispatch } =
    getStoreAccessors<AlbumsState, RootState>('albums'); // We pass namespace here, if we make the module namespaced: true.

export const getAlbums = read(albums.getters.getAlbums);
export const commitSetAlbum = commit(albums.mutations.setAlbum);
