import { AlbumsState } from './albums/state';
import {LayoutState} from './layout/state';

export interface State {
    albums: AlbumsState;
    layout: LayoutState;
}
