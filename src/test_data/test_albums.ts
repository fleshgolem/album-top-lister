import {Album} from '@/data_types/album';
import { Deserialize } from 'cerialize';

export function getTestAlbums(): Promise<Album[][]> {
    return fetch('/albums.json').then((r) => {
        return r.json();
    }).then((r) => {
        const a: Album[][] = Deserialize(r, Album);
        return a;
    });
}
