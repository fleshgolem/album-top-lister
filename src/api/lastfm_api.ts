import {LFMResponse} from '@/data_types/lastfm';
import { Deserialize } from 'cerialize';
import keys from '@/secrets/keys';

export class LastFMAPI {
    public static searchAlbums(query: string): Promise<LFMResponse> {
        // tslint:disable-next-line:max-line-length
        const url = `https://ws.audioscrobbler.com/2.0/?method=album.search&album=${query}&api_key=${keys.lastfm}&format=json`;
        return fetch(url, {
            headers: new Headers({
                'User-Agent': 'Top Album Chart Maker THingy',
            }),
        }).then((r) => {
            return r.json();
        }).then((r) => {
            return Deserialize(r, LFMResponse);
        });
    }
}
