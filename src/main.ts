import Vue from 'vue';
import Vuex from 'vuex';
import App from './App.vue';
import {createStore} from './store';
import BootstrapVue from 'bootstrap-vue';
import './bootstrap';
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap-vue/dist/bootstrap-vue.css';

Vue.use(BootstrapVue);
Vue.use(Vuex);
Vue.config.productionTip = false;
const store = createStore();
new Vue({
  store,
  render: (h) => h(App),
}).$mount('#app');
